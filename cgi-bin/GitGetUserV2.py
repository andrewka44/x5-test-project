# === Тестовое задание для X5 retail by @Andrewka44 === #
# === V2 от 01.09.2020 - Версия с возможностью выбора проекта ===
#!/usr/bin/env python3
import json
import requests
from bs4 import BeautifulSoup
import re
import cgi, cgitb

# === Переменные ===

headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 'accept': '*/*'}
form = cgi.FieldStorage()
projeсt = form.getvalue ('url')

# === Функции ===

def get_html (projeсt, params=None): # Получаем страничку для парсера
    r = requests.get(projeсt, headers=headers, params=params)
    return r

def get_content (html): # Парсим файл, извлекаем айди проекта и получаем пользователей в JSON
    soup = BeautifulSoup(html, 'html.parser')
    items = soup.find_all(class_='home-panel-metadata d-flex flex-wrap text-secondary')
    id = ()
    for item in items:  # Ищем строку Projekt ID
        id = (item.find('', class_='text-secondary'))
    pid  = re.findall(r'\d+', str(id)) # Очищаем строку от букв и оставляем только цифры
    pid = str([int(i) for i in pid])
    get_user = requests.get('https://gitlab.com/api/v4/projects/' + pid[1:-1] + '/users')
    json_data = get_user.json()
    print("Content-type: application/json")
    print()
    print(json_data)

def parse ():
    html = get_html(projeсt)
    get_content(html.text)

# === Функции. Конец ===
parse()
