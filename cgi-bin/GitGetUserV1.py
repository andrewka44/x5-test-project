# === Тестовое задание для X5 retail by @Andrewka44 === #
# === ver 1.0. 29.08.2020 === Без возможности выбора проекта
# === upd от 01.09.2020 === Сделал возможность вставки id в адресную строку. Например: www.sitename.com/cgi-bin/GitGetUserV1.py?id=%PROJECTID
#!/usr/bin/env python3
import json
import requests
import cgi, cgitb

form = cgi.FieldStorage()
get_id = form.getvalue ('id')
project = requests.get('https://gitlab.com/api/v4/projects/'+get_id+'/users')
json_data = project.json()

print("Content-type: application/json")
print()
print(json_data)
